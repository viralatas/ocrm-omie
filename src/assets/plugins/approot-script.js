$(document).ready(function() {

    $('.side-txt').on('click', function() {
        // Desativo fonte negrito anterior, se existe
        $('.active').removeClass('active');
        // Ativa para hyperlink
        $(this).toggleClass('active');
    });

    $('#sidebarCollapse').on('click', function() {

        // Encolher/expandir sidebar e lista
        $('#sidebar').toggleClass('active');
        $('.padrao').toggle();
        $('.mini').toggle();

        // Mudar tamanho da foto
        if ($('.foto').css('height') === '125px') {
            $('.foto').css({
                height: '73px',
                width: '73px',
            });
        } else {
            $('.foto').css({
                height: '125px',
                width: '125px',
            });
        }

        // Mover botão (e inverter seta) quando clicado
        if ($(this).css('marginLeft') === '196px') {
            $(this).css({
                marginLeft: '55px'
            });
        } else {
            $(this).css({
                marginLeft: '196px'
            });
        }
    });
});