import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from "./pages/login/login.component";
import { HomeComponent } from "./pages/home/home.component";

const routes: Routes = [

    {
      path: 'login',
      component: LoginComponent,
      data: { header: 'Login', title: 'Ocrm | Login' },
    },
    {
      path: '',
      component: HomeComponent,
      data: { header: 'Dashboard', title: 'Ocrm | Home' },
    //   canActivate: [AuthGuard]
    },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
  ];

  @NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
