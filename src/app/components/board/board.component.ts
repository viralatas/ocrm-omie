import { Component, OnInit } from '@angular/core';
import { CardStore } from '../cardstore';
import { ListSchema } from '../listschema';

@Component({
    selector: 'app-board',
    templateUrl: './board.component.html',
    styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
    cardStore: CardStore;
    lists: ListSchema[];
    constructor() { }
    setMockData(): void {
        this.cardStore = new CardStore();
        const lists: ListSchema[] = [
            {
                name: 'À Fazer',
                cards: []
            },
            {
                name: 'Em Andamento',
                cards: []
            },
            {
                name: 'Pronto',
                cards: []
            }
        ]
        this.lists = lists;
    }

    ngOnInit() {
        this.setMockData();
    }

}
